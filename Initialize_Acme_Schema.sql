/******************************************************************************
**  Name: Script SQL Data Base Initialize "Acme"
**
**  Authors:	Marvin Dickson Mendia Calizaya
**
**  Date: 06/19/2018
*******************************************************************************
**                            Change History
*******************************************************************************
**   Date:          Author:                         Description:
** --------     -------------     ---------------------------------------------
** 06/19/2018   Marvin Mendia		Initial version
*******************************************************************************/
USE Acme;

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN TRANSACTION;

PRINT 'Insert data into the Area table...';
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('Elc-01','Electricity', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('hew-01','Heavy Work', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('fwo-01','Fine Work', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('roo-01','Roofed', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('fro-01','Fake Roof', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('iog-01','Installation of Gas', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('flo-01','Flooring', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('ins-01','Inspection', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('wsp-01','Work Supervision', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('hid-01','Hidrosanitary', 100);
PRINT 'Area table done...';

PRINT 'Insert data into the Training table...';
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc001','Industrial Electricity','Ing. Rocabado Marcelo',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc002','Home Electricity','Ing. Rocabado Marcelo',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('rco001','Reinforced Concrete','Ing. Flores Antonio',2,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('hid002','Hidrosanitary Normative','Arq. Romero Luis',10,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc003','Industrial Electricity upgrade','Ing. Campero Jose',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc004','Home Eletricity Normative','Ing. Daza Enrique',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('igs001','Installation of Gas Normative','Ing. Romero Antonio',6,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('far001','Fake Roof Upgrade','Arq. Gutierres Maria',5,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('qa001','QA','Ing. Fernandez Rolando',8,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('rco002','Reinforced Concrete Normative','Ing. Flores Antonio',2,100);
PRINT 'Training table done...';


PRINT 'Insert data into the Role table...';
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hem-001','Home Electicity Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hes-001','Home Electicity Supervisor',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('opp-001','Operator',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('het-001','Electicity Technical',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('het-002','Home Electicity Technical',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('iem-001','Industrial Electicity Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hwm-001','Heavy Work Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hws-001','Fine Work Supervisor',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('him-001','Hidrosanitary Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('his-001','Hidrosanitary Supervisor',100);
PRINT 'Role table done...';

PRINT 'Insert data into the Position table...';
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Electricity Manager',1,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Electricity Supervisor',2,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Instal Hidrosanitary',3,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Hidrosanitary Manager',9,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Hidrosanitary Supervisor',10,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Operator Heavy Work',3,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Operator Electricity',1,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Fine Work Manager',8,100);
PRINT 'Position table done...';

PRINT 'Insert data into the Employee_Training table...';
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (1,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (1,2,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (2,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (3,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (4,2,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (5,5,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (1,3,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (3,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (2,2,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (3,5,'ACTIVO',100);
PRINT 'Employee_Training table done...';

COMMIT TRANSACTION;