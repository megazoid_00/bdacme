/******************************************************************************
**				   	        CREATION OF STORED PROCEDURES	   				 **
*******************************************************************************/
/******************************************************************************
**  Author: Marvin Mendia													 **
**  Date: 06/19/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 06/19/2018		Marvin Mendia			Initial version					 **
*******************************************************************************/
USE Acme
GO
PRINT 'Start of Script Execution....';
GO
/******************************************************************************
**				 Name: SP_InsertArea										 **
**				 Desc: Insert Register Area table							 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertArea]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_InsertArea]
END
GO
CREATE PROCEDURE [dbo].[SP_InsertArea]
(
	@Code VARCHAR(10),
	@Name VARCHAR(50),
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN

	INSERT INTO [dbo].[Area](code 
							,name 
							,ModifiedBy)
	VALUES (@Code
			,@Name
			,@ModifiedBy);
	SELECT @@IDENTITY AS NewAraeaId;
END
GO
PRINT 'Procedure [dbo].[SP_InsertArea] created';
GO
/******************************************************************************
**				 Name: SP_UpdateArea										 **
**				 Desc: Update Register Area table							 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		   WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateArea]') 
		   AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_UpdateArea];
END
GO
CREATE PROCEDURE [dbo].[SP_UpdateArea]
(
	@IdArea INT,
	@Code VARCHAR(10),
	@Name VARCHAR(50),
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	UPDATE [dbo].[Area]
	SET update_On = GETDATE()
		,code = @Code
		,name = @Name
		,ModifiedBy = @ModifiedBy
	WHERE Id = @IdArea;

	IF @@ROWCOUNT > 0
	 BEGIN
		SELECT code
		FROM [dbo].[Area] 
		WHERE Id = @IdArea;
	 END
END
GO
PRINT 'Procedure [dbo].[SP_UpdateArea] created';
GO
/******************************************************************************
**				 Name: SP_DeleteArea										 **
**				 Desc: Delete Register Area table							 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeleteArea]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_DeleteArea];
END
GO
CREATE PROCEDURE [dbo].[SP_DeleteArea]
(
	@IdArea INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	DELETE FROM [dbo].[Area]
	WHERE Id = @IdArea;
END
GO
PRINT 'Procedure [dbo].[SP_DeleteArea] created';
GO

/******************************************************************************
**				 Name: SP_GetAreaId											 **
**				 Desc: Get Area by Id table									 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetAreaId]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetAreaId];
END
GO
CREATE PROCEDURE [dbo].[SP_GetAreaId]
(
	@IdArea INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT code
		  ,name
    FROM [dbo].[Area]
	WHERE Id = @IdArea;
END
GO
PRINT 'Procedure [dbo].[SP_GetAreaId] created';
GO
/******************************************************************************
**				 Name: SP_GetArea											 **
**				 Desc: Get Area	table										 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetArea]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetArea];
END
GO
CREATE PROCEDURE [dbo].[SP_GetArea]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT code
		  ,name
    FROM [dbo].[Area];
END
GO
PRINT 'Procedure [dbo].[SP_GetArea] created';
GO
/******************************************************************************
**				 Name: SP_InsertTraining									 **
**				 Desc: Insert Register Training table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_InsertTraining]
END
GO
CREATE PROCEDURE [dbo].[SP_InsertTraining]
(
	@Code VARCHAR(10),
	@Name VARCHAR(30),
	@Instructor VARCHAR(50),
	@Area_Id INT,
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN

	INSERT INTO [dbo].[Training](code 
							,name 
							,instructor
							,area_id
							,ModifiedBy)
	VALUES (@Code
			,@Name
			,@Instructor
			,@Area_Id
			,@ModifiedBy);
	SELECT @@IDENTITY AS NewTrainingId;
END
GO
PRINT 'Procedure [dbo].[SP_InsertTraining] created';
GO
/******************************************************************************
**				 Name: SP_UpdateTraining									 **
**				 Desc: Update Register Training table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_UpdateTraining]
END
GO
CREATE PROCEDURE [dbo].[SP_UpdateTraining]
(
	@IdTraining INT,
	@Code VARCHAR(10),
	@Name VARCHAR(50),
	@Instructor VARCHAR(50),
	@Area_Id INT,
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	UPDATE [dbo].[Training]
	SET update_On = GETDATE()
		,code = @Code
		,name = @Name
		,instructor =@Instructor
		,area_id = @Area_Id
		,ModifiedBy = @ModifiedBy
	WHERE Id = @IdTraining;

	IF @@ROWCOUNT > 0
	 BEGIN
		SELECT code
		FROM [dbo].[Training]
		WHERE Id = @IdTraining;
	 END
END
GO
PRINT 'Procedure [dbo].[SP_UpdateTraining] created';
GO
/******************************************************************************
**				 Name: SP_DeleteTraining									 **
**				 Desc: Delete Register Training table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeleteTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_DeleteTraining]
END
GO
CREATE PROCEDURE [dbo].[SP_DeleteTraining]
(
	@IdTraining INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	DELETE FROM [dbo].[Training] 
	WHERE Id = @IdTraining;
END
GO
PRINT 'Procedure [dbo].[SP_DeleteTraining] created';
GO
/******************************************************************************
**				 Name: SP_GetTrainingId										 **
**				 Desc: Get Training by Id table								 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetTrainingId]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetTrainingId];
END
GO
CREATE PROCEDURE [dbo].[SP_GetTrainingId]
(
	@IdTraining INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT t.code
		  ,t.name
		  ,t.instructor
		  ,a.name
    FROM [dbo].[Training] t
	INNER JOIN [dbo].[Area] a
	ON (t.area_id = a.Id)
	WHERE t.Id = @IdTraining;
END
GO
PRINT 'Procedure [dbo].[SP_GetTrainingId] created';
GO
/******************************************************************************
**				 Name: SP_GetTraining										 **
**				 Desc: Get Training table									 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetTraining];
END
GO
CREATE PROCEDURE [dbo].[SP_GetTraining]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT t.code
		  ,t.name
		  ,t.instructor
		  ,a.name
    FROM [dbo].[Training] t
	INNER JOIN [dbo].[Area] a
	ON (t.area_id = a.Id);
END
GO
PRINT 'Procedure [dbo].[SP_GetTraining] created';
GO
/******************************************************************************
**				 Name: SP_InsertRole										 **
**				 Desc: Insert Register Role table							 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertRole]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_InsertRole]
END
GO
CREATE PROCEDURE [dbo].[SP_InsertRole]
(
	@Code VARCHAR(10),
	@Description VARCHAR(150),
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN

	INSERT INTO [dbo].[Role](code 
							,[description]
							,ModifiedBy)
	VALUES (@Code
			,@Description
			,@ModifiedBy);
	SELECT @@IDENTITY AS NewRoleId;
END
GO
PRINT 'Procedure [dbo].[SP_InsertRole] created';
GO
/******************************************************************************
**				 Name: SP_UpdateRole										 **
**				 Desc: Update Register Role table							 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateRole]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_UpdateRole]
END
GO
CREATE PROCEDURE [dbo].[SP_UpdateRole]
(
	@IdRole INT,
	@Code VARCHAR(10),
	@Description VARCHAR(150),
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	UPDATE [dbo].[Role] 
	SET update_On = GETDATE()
		,code = @Code
		,[description] = @Description
		,ModifiedBy = @ModifiedBy
	WHERE Id = @IdRole;

	IF @@ROWCOUNT > 0
	 BEGIN
		SELECT code
		FROM [dbo].[Role] 
		WHERE Id = @IdRole;
	 END
END
GO
PRINT 'Procedure [dbo].[SP_UpdateRole] created';
GO
/******************************************************************************
**				 Name: SP_DeleteRole										 **
**				 Desc: Delete Register Role table							 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeleteRole]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_DeleteRole]
END
GO
CREATE PROCEDURE [dbo].[SP_DeleteRole]
(
	@IdRole INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	DELETE FROM [dbo].[Role] 
	WHERE Id = @IdRole;
END
GO
PRINT 'Procedure [dbo].[SP_DeleteRole] created';
GO
/******************************************************************************
**				 Name: SP_GetRoleId											 **
**				 Desc: Get Role by Id table									 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRoleId]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetRoleId];
END
GO
CREATE PROCEDURE [dbo].[SP_GetRoleId]
(
	@IdRole INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT code
		  ,[description]
    FROM [dbo].[Role]
	WHERE Id = @IdRole;
END
GO
PRINT 'Procedure [dbo].[SP_GetRoleId] created';
GO
/******************************************************************************
**				 Name: SP_GetRole											 **
**				 Desc: Get Role table										 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRole]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetRole];
END
GO
CREATE PROCEDURE [dbo].[SP_GetRole]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT code
		  ,[description]
    FROM [dbo].[Role];
END
GO
PRINT 'Procedure [dbo].[SP_GetRole] created';
GO
/******************************************************************************
**				 Name: SP_InsertPosition									 **
**				 Desc: Insert Register Position table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertPosition]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_InsertPosition]
END
GO
CREATE PROCEDURE [dbo].[SP_InsertPosition]
(
	@Name VARCHAR(30),
	@Role_Id INT,
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN

	INSERT INTO [dbo].[Position](name 
							,Role_id
							,ModifiedBy)
	VALUES (@Name
			,@Role_Id
			,@ModifiedBy);
	SELECT @@IDENTITY AS NewPositionId;
END
GO
PRINT 'Procedure [dbo].[SP_InsertPosition] created';
GO
/******************************************************************************
**				 Name: SP_UpdatePosition									 **
**				 Desc: Update Register Position table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdatePosition]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_UpdatePosition]
END
GO
CREATE PROCEDURE [dbo].[SP_UpdatePosition]
(
	@IdPosition INT,
	@Name VARCHAR(50),
	@Role_Id INT,
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	UPDATE [dbo].[Position]
	SET update_On = GETDATE()
		,name = @Name
		,role_id = @Role_Id
		,ModifiedBy = @ModifiedBy
	WHERE Id = @IdPosition;

	IF @@ROWCOUNT > 0
	 BEGIN
		SELECT name
		FROM [dbo].[Position]
		WHERE Id = @IdPosition;
	 END
END
GO
PRINT 'Procedure [dbo].[SP_UpdatePosition] created';
GO
/******************************************************************************
**				 Name: SP_DeletePosition									 **
**				 Desc: Delete Register Position table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeletePosition]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_DeletePosition]
END
GO
CREATE PROCEDURE [dbo].[SP_DeletePosition]
(
	@IdPosition INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	DELETE FROM [dbo].[Position] 
	WHERE Id = @IdPosition;
END
GO
PRINT 'Procedure [dbo].[SP_DeletePosition] created';
GO
/******************************************************************************
**				 Name: SP_GetPositionId										 **
**				 Desc: Get Position by Id table								 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetPositionId]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetPositionId];
END
GO
CREATE PROCEDURE [dbo].[SP_GetPositionId]
(
	@IdPosition INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT p.name
		  ,r.[description]
    FROM [dbo].[Position] p
	INNER JOIN [dbo].[Role] r
	ON (p.role_id = r.Id)
	WHERE p.Id = @IdPosition;
END
GO
PRINT 'Procedure [dbo].[SP_GetPositionId] created';
GO
/******************************************************************************
**				 Name: SP_GetPosition										 **
**				 Desc: Get Position table									 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetPosition]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetPosition];
END
GO
CREATE PROCEDURE [dbo].[SP_GetPosition]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT p.name
		  ,r.[description]
    FROM [dbo].[Position] p
	INNER JOIN [dbo].[Role] r
	ON (p.role_id = r.Id);
END
GO
PRINT 'Procedure [dbo].[SP_GetPosition] created';
GO

/******************************************************************************
**				 Name: SP_InsertEmployeeTraining							 **
**				 Desc: Insert Register Employee_Training table				 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertEmployeeTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_InsertEmployeeTraining]
END
GO
CREATE PROCEDURE [dbo].[SP_InsertEmployeeTraining]
(
	@Employee_Id INT,
	@Training_Id INT,
	@State VARCHAR(10),
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN

	INSERT INTO [dbo].[Employee_Training](employee_id 
										  ,training_id
										  ,[state]
										  ,ModifiedBy)
	VALUES (@Employee_Id
			,@Training_Id
			,@State
			,@ModifiedBy);
	SELECT @@IDENTITY AS NewEmployeeTrainingId;
END
GO
PRINT 'Procedure [dbo].[SP_InsertEmployeeTraining] created';
GO
/******************************************************************************
**				 Name: SP_UpdateEmployeeTraining							 **
**				 Desc: Update Register Employee_Training table				 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateEmployeeTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_UpdateEmployeeTraining]
END
GO
CREATE PROCEDURE [dbo].[SP_UpdateEmployeeTraining]
(
	@IdEmployeeTraining INT,
	@Employee_Id INT,
	@Training_Id INT,
	@State VARCHAR(10),
	@ModifiedBy INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	UPDATE [Employee_Training]
	SET update_On = GETDATE()
		,employee_id = @Employee_Id
		,training_id = @Training_Id
		,ModifiedBy = @ModifiedBy
	WHERE Id = @IdEmployeeTraining;

	IF @@ROWCOUNT > 0
	 BEGIN
		SELECT Id
		FROM [dbo].[Employee_Training]
		WHERE Id = @IdEmployeeTraining;
	 END
END
GO
PRINT 'Procedure [dbo].[SP_UpdateEmployeeTraining] created';
GO
/******************************************************************************
**				 Name: SP_DeleteEmployeeTraining							 **
**				 Desc: Delete Register Employee_Training table				 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeleteEmployeeTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_DeleteEmployeeTraining]
END
GO
CREATE PROCEDURE [dbo].[SP_DeleteEmployeeTraining]
(
	@IdEmployeeTraining INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	DELETE FROM [dbo].[Employee_Training] 
	WHERE Id = @IdEmployeeTraining;
END
GO
PRINT 'Procedure [dbo].[SP_DeleteEmployeeTraining] created';
GO
/******************************************************************************
**				 Name: SP_GetEmployeeTrainingId								 **
**				 Desc: Get Employee_Training by Id table					 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetEmployeeTrainingId]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetEmployeeTrainingId];
END
GO
CREATE PROCEDURE [dbo].[SP_GetEmployeeTrainingId]
(
	@IdEmployeeTraining INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT Training = t.name
		  ,Instructor = t.instructor
		  ,Area = a.name
		  --,Employee = e.firstName + ' ' + e. lastName
    FROM [dbo].[Employee_Training] et
	--INNER JOIN [dbo].[Employee] e
	--ON (et.employee_id = e.Id)
	INNER JOIN [dbo].[Training] t
	ON (et.training_id = t.Id)
	INNER JOIN [dbo].[Area] a
	ON (t.area_id = a.Id)
	WHERE et.Id = @IdEmployeeTraining;
END
GO
PRINT 'Procedure [dbo].[SP_GetEmployeeTrainingId] created';
GO
/******************************************************************************
**				 Name: SP_GetEmployeeTraining								 **
**				 Desc: Get Employee_Training table							 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetEmployeeTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_GetEmployeeTraining];
END
GO
CREATE PROCEDURE [dbo].[SP_GetEmployeeTraining]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT Training = t.name
		  ,Instructor = t.instructor
		  ,Area = a.name
		  --,Employee = e.firstName + ' ' + e. lastName
    FROM [dbo].[Employee_Training] et
	--INNER JOIN [dbo].[Employee] e
	--ON (et.employee_id = e.Id)
	INNER JOIN [dbo].[Training] t
	ON (et.training_id = t.Id)
	INNER JOIN [dbo].[Area] a
	ON (t.area_id = a.Id);
END
GO
PRINT 'Procedure [dbo].[SP_GetEmployeeTraining] created';
GO

PRINT 'End of Script Execution....';
GO